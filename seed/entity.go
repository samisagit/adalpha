package seed

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

type Investment struct {
	ISIN  string
	Asset string
	POP   float64
}

type Investments []Investment

var SeededInvestments Investments

func InitialisePortfolioData() {
	seedFile, err := os.Open("/config/init.csv")
	if err != nil {
		fmt.Print(err)
	}
	reader := csv.NewReader(bufio.NewReader(seedFile))
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		pop, err := strconv.ParseFloat(line[2], 64)
		if err != nil {
			log.Fatal(err)
		}
		SeededInvestments = append(SeededInvestments, Investment{
			ISIN:  line[0],
			Asset: line[1],
			POP:   pop,
		})
	}
}
