package portfolio

import (
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnitSortAssets(t *testing.T) {
	cases := []struct {
		name               string
		inputPort          *Container
		expectedAssetOrder []string
	}{
		{
			name: "random",
			inputPort: &Container{
				Assets: &[]Asset{
					Asset{
						ISIN:  "1",
						Value: 100.00,
					},
					Asset{
						ISIN:  "2",
						Value: 50.99,
					},
					Asset{
						ISIN:  "3",
						Value: 230.21,
					},
					Asset{
						ISIN:  "4",
						Value: 190.56,
					},
				},
				m: &sync.Mutex{},
			},
			expectedAssetOrder: []string{"2", "1", "4", "3"},
		},
		{
			name: "desc",
			inputPort: &Container{
				Assets: &[]Asset{
					Asset{
						ISIN:  "3",
						Value: 230.21,
					},
					Asset{
						ISIN:  "4",
						Value: 190.56,
					},
					Asset{
						ISIN:  "1",
						Value: 100.00,
					},
					Asset{
						ISIN:  "2",
						Value: 50.99,
					},
				},
				m: &sync.Mutex{},
			},
			expectedAssetOrder: []string{"2", "1", "4", "3"},
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			if len(*val.inputPort.Assets) != len(val.expectedAssetOrder) {
				t.Errorf("expected asset length to be same as expected order")
				t.FailNow()
			}
			val.inputPort.sortAssets()
			al := *val.inputPort.Assets
			for i, expectedISIN := range val.expectedAssetOrder {
				if expectedISIN != al[i].ISIN {
					t.Errorf(
						"assets at index %d are different: expected %s, result %s",
						i,
						expectedISIN,
						al[i].ISIN,
					)
				}
			}
		})
	}
}

func TestUnitLiquidate(t *testing.T) {
	cases := []struct {
		name            string
		inputPort       Container
		ISINToRaise     string
		expectedBalance float64
		expectedAssets  []string
		errExpected     error
	}{
		{
			name: "valid ISIN",
			inputPort: Container{
				Balance: 0,
				Assets: &[]Asset{
					Asset{
						ISIN:  "3",
						Value: 230.21,
					},
					Asset{
						ISIN:  "4",
						Value: 190.56,
					},
					Asset{
						ISIN:  "1",
						Value: 100.00,
					},
					Asset{
						ISIN:  "2",
						Value: 50.99,
					},
				},
				m: &sync.Mutex{},
			},
			ISINToRaise:     "1",
			expectedBalance: 100.00,
			expectedAssets:  []string{"3", "4", "2"},
			errExpected:     nil,
		},
		{
			name: "invalid ISIN",
			inputPort: Container{
				Balance: 0,
				Assets: &[]Asset{
					Asset{
						ISIN:  "3",
						Value: 230.21,
					},
					Asset{
						ISIN:  "4",
						Value: 190.56,
					},
					Asset{
						ISIN:  "1",
						Value: 100.00,
					},
					Asset{
						ISIN:  "2",
						Value: 50.99,
					},
				},
				m: &sync.Mutex{},
			},
			ISINToRaise:     "5",
			expectedBalance: 0,
			expectedAssets:  []string{"3", "4", "1", "2"},
			errExpected:     fmt.Errorf("no asset with specified ISIN %s", "5"),
		},
		{
			name: "with existing Balance",
			inputPort: Container{
				Balance: 100,
				Assets: &[]Asset{
					Asset{
						ISIN:  "3",
						Value: 230.21,
					},
					Asset{
						ISIN:  "4",
						Value: 190.56,
					},
					Asset{
						ISIN:  "1",
						Value: 100.00,
					},
					Asset{
						ISIN:  "2",
						Value: 50.99,
					},
				},
				m: &sync.Mutex{},
			},
			ISINToRaise:     "1",
			expectedBalance: 200.00,
			expectedAssets:  []string{"3", "4", "2"},
			errExpected:     nil,
		},
		{
			name: "with negative Balance",
			inputPort: Container{
				Balance: -100,
				Assets: &[]Asset{
					Asset{
						ISIN:  "3",
						Value: 230.21,
					},
					Asset{
						ISIN:  "4",
						Value: 190.56,
					},
					Asset{
						ISIN:  "1",
						Value: 100.00,
					},
					Asset{
						ISIN:  "2",
						Value: 50.99,
					},
				},
				m: &sync.Mutex{},
			},
			ISINToRaise:     "4",
			expectedBalance: 90.56,
			expectedAssets:  []string{"3", "1", "2"},
			errExpected:     nil,
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			err := val.inputPort.liquidate(val.ISINToRaise)
			if err != val.errExpected {
				if val.errExpected == nil {
					t.Errorf("expected err to be nil, go %v", err)
				} else if val.errExpected != nil && err.Error() != val.errExpected.Error() {
					t.Errorf("expected error with value %s got %s", val.errExpected.Error(), err.Error())
				}
			}

			if val.expectedBalance != val.inputPort.Balance {
				t.Errorf("expected Balance to be %f, got %f", val.expectedBalance, val.inputPort.Balance)
			}

			al := *val.inputPort.Assets
			if len(al) != len(val.expectedAssets) {
				t.Errorf("expected %d assets left, got %d", len(val.expectedAssets), len(al))
				t.FailNow()
			}

			for i, expectedISIN := range val.expectedAssets {
				if expectedISIN != al[i].ISIN {
					t.Errorf(
						"assets at index %d are different: expected %s, result %s",
						i,
						expectedISIN,
						al[i].ISIN,
					)
				}

			}
		})
	}
}

func TestUnitLiquidationOptions(t *testing.T) {
	cases := []struct {
		name            string
		inputPort       Container
		fundsNeeded     float64
		errExpected     error
		matchesExpected [][]Asset
	}{
		{
			name: "happy path",
			inputPort: Container{
				Assets: &[]Asset{
					Asset{
						Value: 1,
					},
					Asset{
						Value: 3,
					},
					Asset{
						Value: 5,
					},
					Asset{
						Value: 8,
					},
					Asset{
						Value: 9,
					},
				},
				m: &sync.Mutex{},
			},
			fundsNeeded: 9,
			matchesExpected: [][]Asset{
				{
					{
						Value: 1,
					},
					{
						Value: 3,
					},
					{
						Value: 5,
					},
				},
				{
					{
						Value: 1,
					},
					{
						Value: 8,
					},
				},
				{
					{
						Value: 9,
					},
				},
			},
		},
		{
			name: "no exact match",
			inputPort: Container{
				Assets: &[]Asset{
					Asset{
						Value: 1,
					},
					Asset{
						Value: 3,
					},
					Asset{
						Value: 5,
					},
					Asset{
						Value: 8,
					},
					Asset{
						Value: 9,
					},
				},
				m: &sync.Mutex{},
			},
			fundsNeeded: 2,
			matchesExpected: [][]Asset{
				{
					{
						Value: 3,
					},
				},
			},
			errExpected: fmt.Errorf("no exact match"),
		},
		{
			name: "no match",
			inputPort: Container{
				Assets: &[]Asset{
					Asset{
						Value: 1,
					},
					Asset{
						Value: 3,
					},
					Asset{
						Value: 5,
					},
					Asset{
						Value: 8,
					},
					Asset{
						Value: 9,
					},
				},
				m: &sync.Mutex{},
			},
			fundsNeeded: 40000,
			errExpected: fmt.Errorf("no exact match"),
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			options, err := val.inputPort.liquidationOptions(val.fundsNeeded)
			if err != nil {
				if err.Error() != val.errExpected.Error() {
					t.Errorf("expected error %s, got %s", val.errExpected.Error(), err.Error())
				}
			}
			if val.errExpected != nil && err == nil {
				t.Errorf("expected error %s, got nil", val.errExpected.Error())
			}

			if !assert.Equal(t, *options, val.matchesExpected) {
				t.Errorf("expected %v, got %v", val.matchesExpected, *options)
			}
		})
	}
}
