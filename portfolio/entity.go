package portfolio

import (
	"fmt"
	"math"
	"sort"
	"sync"
)

type Asset struct {
	ISIN  string
	Name  string
	Value float64
}

type Container struct {
	Balance float64
	Assets  *[]Asset
	m       *sync.Mutex
}

func (c *Container) sortAssets() {
	c.m.Lock()
	defer c.m.Unlock()

	al := *c.Assets
	sort.Slice(al, func(i, j int) bool {
		return al[i].Value < al[j].Value
	})

	c.Assets = &al
}

func (c *Container) liquidate(ISIN string) error {
	c.m.Lock()
	defer c.m.Unlock()

	al := *c.Assets
	for i, a := range al {
		if a.ISIN == ISIN {
			al = append(al[:i], al[i+1:]...)
			c.Balance += a.Value
			c.Assets = &al
			return nil
		}
	}

	return fmt.Errorf("no asset with specified ISIN %s", ISIN)
}

func (c *Container) liquidationOptions(fundsNeeded float64) (*[][]Asset, error) {
	c.m.Lock()
	defer c.m.Unlock()
	thisAssets := *c.Assets

	var closestMatches [][]Asset
	var closestDiff = math.MaxFloat64
	combos := assetPermutations(thisAssets)

	for _, val := range combos {
		comboTotal := AssetComboValue(val)
		diff := comboTotal - fundsNeeded

		if diff < 0 {
			continue
		}

		switch true {
		case diff < closestDiff:
			closestDiff = diff
			closestMatches = [][]Asset{
				val,
			}
		case diff == closestDiff:
			closestMatches = append(closestMatches, val)
		}
	}

	var err error
	if closestDiff > 0 {
		err = fmt.Errorf("no exact match")
	}

	return &closestMatches, err

}
