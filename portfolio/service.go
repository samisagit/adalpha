package portfolio

import "fmt"

func ByID(ID string) (*Container, error) {
	c, ok := seedStore[ID]
	if !ok {
		return nil, fmt.Errorf("no portfolio of ID %s exists", ID)
	}
	return c, nil
}

func assetPermutations(set []Asset) [][]Asset {

	length := uint(len(set))
	subsets := [][]Asset{}

	for subsetBits := 1; subsetBits < (1 << length); subsetBits++ {
		var subset []Asset

		for i := uint(0); i < length; i++ {
			if (subsetBits>>i)&1 == 1 {
				subset = append(subset, set[i])
			}
		}
		subsets = append(subsets, subset)
	}
	return subsets
}

func AssetComboValue(set []Asset) float64 {
	var total float64
	for _, asset := range set {
		total += asset.Value
	}

	return total
}

func GenerateWithdrawalOptions(ID string, fundsNeeded float64) (*[][]Asset, error) {
	pc, err := ByID(ID)
	if err != nil {
		return nil, err
	}

	return pc.liquidationOptions(fundsNeeded)
}
