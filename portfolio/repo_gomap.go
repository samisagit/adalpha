package portfolio

import (
	"adalpha-api/seed"
	"fmt"
	"sync"
)

const totalPortfolioValue float64 = 220000

func InitialisePortfolioStore() {
	var sa []Asset
	for _, val := range seed.SeededInvestments {
		sa = append(
			sa,
			Asset{
				ISIN:  val.ISIN,
				Name:  val.Asset,
				Value: (totalPortfolioValue / 100) * val.POP,
			},
		)
	}

	seedStore = store{
		"testExample": &Container{
			Balance: 0,
			Assets:  &sa,
			m:       &sync.Mutex{},
		},
	}

}

var seedStore store

type store map[string]*Container

func (s store) Get(ID string) (*Container, error) {
	if c, ok := s[ID]; !ok {
		return nil, fmt.Errorf("no portfolio of ID %s exists", ID)
	} else {
		return c, nil
	}
}
