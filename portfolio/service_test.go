package portfolio

import (
	"fmt"
	"math"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnitByID(t *testing.T) {
	// set up and clean up
	preSeedStore := seedStore
	defer func() { seedStore = preSeedStore }()
	seedStore = store{
		"testID": &Container{
			Assets: &[]Asset{
				Asset{
					ISIN:  "testISIN",
					Value: 21.60,
				},
			},
		},
		"highBalance": &Container{
			Balance: math.MaxFloat64,
		},
	}

	cases := []struct {
		name        string
		outputPort  *Container
		inputID     string
		errExpected error
	}{
		{
			name:    "happy path",
			inputID: "testID",
			outputPort: &Container{
				Assets: &[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 21.60,
					},
				},
			},
			errExpected: nil,
		},
		{
			name:    "high balance",
			inputID: "highBalance",
			outputPort: &Container{
				Balance: math.MaxFloat64,
			},
			errExpected: nil,
		},
		{
			name:        "invalid ID",
			inputID:     "fakeID",
			errExpected: fmt.Errorf("no portfolio of ID %s exists", "fakeID"),
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			pc, err := ByID(val.inputID)
			if err != nil {
				if err.Error() != val.errExpected.Error() {
					t.Errorf("expected error %s, got %s", val.errExpected.Error(), err.Error())
				}
			} else {
				if val.errExpected != nil {
					t.Errorf("expected error with value, got nil")
				}
			}

			if pc != nil {
				if val.outputPort == nil {
					t.Errorf("expected outputPort to be nil, got %v", *pc)
					t.FailNow()
				}
				if !assert.Equal(t, pc, val.outputPort) {
					t.Errorf("expected container with value %v, got %v", *val.outputPort, *pc)
				}
			}
		})
	}
}

func TestUnitAssetPermutations(t *testing.T) {
	cases := []struct {
		name        string
		inputAssets []Asset
		outputSets  [][]Asset
	}{
		{
			name: "happy path",
			inputAssets: []Asset{
				Asset{
					ISIN: "A",
				},
				Asset{
					ISIN: "B",
				},
			},
			outputSets: [][]Asset{
				[]Asset{
					Asset{ISIN: "A"},
				},
				[]Asset{
					Asset{ISIN: "B"},
				},
				[]Asset{
					Asset{ISIN: "A"},
					Asset{ISIN: "B"},
				},
			},
		},
		{
			name:        "empty input",
			inputAssets: []Asset{},
			outputSets:  [][]Asset{},
		},
		{
			name: "duplicate values",
			inputAssets: []Asset{
				Asset{
					ISIN: "A",
				},
				Asset{
					ISIN: "A",
				},
			},
			outputSets: [][]Asset{
				[]Asset{
					Asset{ISIN: "A"},
				},
				[]Asset{
					Asset{ISIN: "A"},
				},
				[]Asset{
					Asset{ISIN: "A"},
					Asset{ISIN: "A"},
				},
			},
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			sets := assetPermutations(val.inputAssets)

			if !assert.Equal(t, sets, val.outputSets) {
				t.Errorf("expected sets to have value %v, got %v", val.outputSets, sets)
			}
		})
	}
}

func TestUnitAssetComboValue(t *testing.T) {
	cases := []struct {
		name           string
		inputSet       []Asset
		outputExpected float64
	}{
		{
			name: "happy path",
			inputSet: []Asset{
				Asset{
					Value: 10.61,
				},
				Asset{
					Value: 3.41,
				},
			},
			outputExpected: 14.02,
		},
		{
			name:           "empty set",
			outputExpected: 0,
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			sum := AssetComboValue(val.inputSet)
			if sum != val.outputExpected {
				t.Errorf("expected sum to equal %f, got %f", val.outputExpected, sum)
			}
		})
	}
}

func TestIntegrationGenerateWithdrawalOptions(t *testing.T) {
	// set up and clean up
	preSeedStore := seedStore
	defer func() { seedStore = preSeedStore }()
	seedStore = store{
		"testID": &Container{
			Assets: &[]Asset{
				Asset{
					ISIN:  "testISIN",
					Value: 21.60,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 25.40,
				},
			},
			m: &sync.Mutex{},
		},
		"multiple": &Container{
			Assets: &[]Asset{
				Asset{
					ISIN:  "testISIN",
					Value: 21.60,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 25.40,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 21.59,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 25.41,
				},
			},
			m: &sync.Mutex{},
		},
		"partial": &Container{
			Assets: &[]Asset{
				Asset{
					ISIN:  "testISIN",
					Value: 21.60,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 25.40,
				},
			},
			m: &sync.Mutex{},
		},
		"multiplePartial": &Container{
			Assets: &[]Asset{
				Asset{
					ISIN:  "testISIN",
					Value: 2,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 7,
				},
				Asset{
					ISIN:  "testISIN",
					Value: 9,
				},
			},
			m: &sync.Mutex{},
		},
	}

	cases := []struct {
		name             string
		inputID          string
		inputFundsNeeded float64
		setsExpected     [][]Asset
		errExpected      error
	}{
		{
			name:             "exact match",
			inputID:          "testID",
			inputFundsNeeded: 47,
			setsExpected: [][]Asset{
				[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 21.60,
					},
					Asset{
						ISIN:  "testISIN",
						Value: 25.40,
					},
				},
			},
			errExpected: nil,
		},
		{
			name:             "multiple exact matches",
			inputID:          "multiple",
			inputFundsNeeded: 47,
			setsExpected: [][]Asset{
				[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 21.60,
					},
					Asset{
						ISIN:  "testISIN",
						Value: 25.40,
					},
				},
				[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 21.59,
					},
					Asset{
						ISIN:  "testISIN",
						Value: 25.41,
					},
				},
			},
			errExpected: nil,
		},
		{
			name:             "partial match",
			inputID:          "partial",
			inputFundsNeeded: 25,
			setsExpected: [][]Asset{
				[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 25.40,
					},
				},
			},
			errExpected: fmt.Errorf("no exact match"),
		},
		{
			name:             "mulitple partial match",
			inputID:          "multiplePartial",
			inputFundsNeeded: 8,
			setsExpected: [][]Asset{
				[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 2,
					},

					Asset{
						ISIN:  "testISIN",
						Value: 7,
					},
				},
				[]Asset{
					Asset{
						ISIN:  "testISIN",
						Value: 9,
					},
				},
			},
			errExpected: fmt.Errorf("no exact match"),
		},
		{
			name:             "no match",
			inputID:          "testID",
			inputFundsNeeded: 800,
			errExpected:      fmt.Errorf("no exact match"),
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			sets, err := GenerateWithdrawalOptions(val.inputID, val.inputFundsNeeded)
			if err != nil {
				if err.Error() != val.errExpected.Error() {
					t.Errorf("expected error %s, got %s", val.errExpected.Error(), err.Error())
				}
			} else {
				if val.errExpected != nil {
					t.Errorf("expected error with value, got nil")
				}
			}

			if sets == nil {
				if val.setsExpected != nil {
					t.Errorf("expected sets to have value %v, got nil", val.setsExpected)
					t.FailNow()
				}
			}
			if !assert.Equal(t, *sets, val.setsExpected) {
				t.Errorf("expected sets to have value %v, got %v", val.setsExpected, *sets)
			}

		})
	}
}
