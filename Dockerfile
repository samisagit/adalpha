# Building stage
FROM golang:1.11.2 AS builder

ENV GO111MODULE on

WORKDIR /module

COPY go.mod ./

RUN go mod download

COPY . .

RUN go build -o binary ./main

# Running stage
FROM alpine

RUN apk add --no-cache libc6-compat ca-certificates apache2-utils

COPY --from=builder module/binary ./

ENTRYPOINT "./binary"

