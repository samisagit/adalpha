package main

import (
	"adalpha-api/portfolio"
	"adalpha-api/presentation"
	"adalpha-api/seed"
)

func main() {
	seed.InitialisePortfolioData()
	portfolio.InitialisePortfolioStore()
	presentation.InitialiseRouter()
}
