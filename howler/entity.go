package howler

type Code int

const (
	PortfolioNotExist       Code = 456
	NoIDProvided            Code = 914
	CantWriteToJSON         Code = 415
	WithdrawalAmountInvalid Code = 729
)
