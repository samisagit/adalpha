package presentation

import (
	"adalpha-api/howler"
	"adalpha-api/portfolio"
	"encoding/json"
	"net/http"
)

type portfolioData struct {
	Balance float64     `json:"balance"`
	Assets  []assetData `json:"assets"`
}

type assetData struct {
	ISIN  string  `json:"ISIN"`
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

type transactionData struct {
	Asset assetData `json:"asset"`
	Date  string    `json:"date"`
}

type optionData struct {
	Assets     []portfolio.Asset `json:"assets"`
	SetValue   float64           `json:"setValue"`
	ExactMatch bool              `json:"exactMatch"`
}

func portfolioByID(ID string, w http.ResponseWriter) {
	pc, err := portfolio.ByID(ID)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		errRes := generateErrorResponse(howler.PortfolioNotExist)
		b, _ := json.Marshal(errRes)
		w.Write(b)
		return
	}

	resPortfolio := portfolioData{
		Balance: pc.Balance,
		Assets:  make([]assetData, len(*pc.Assets)),
	}

	for i, val := range *pc.Assets {
		resPortfolio.Assets[i] = assetData{
			ISIN:  val.ISIN,
			Name:  val.Name,
			Value: val.Value,
		}

	}

	b, err := json.Marshal(resPortfolio)
	if err != nil {
		errRes := generateErrorResponse(howler.CantWriteToJSON)
		w.WriteHeader(http.StatusInternalServerError)
		b, _ := json.Marshal(errRes)
		w.Write(b)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(b)
	return
}

func withdrawalOptions(fundsNeeded float64, w http.ResponseWriter) {
	// lets assume the user is authenticated and the ID is testExample
	exactMatch := true
	sets, err := portfolio.GenerateWithdrawalOptions("testExample", fundsNeeded)
	if err != nil {
		if err.Error() != "no exact match" {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		exactMatch = false
	}

	cpSets := *sets
	resData := make([]optionData, len(cpSets))
	for i, val := range cpSets {
		resData[i] = optionData{
			Assets:     val,
			SetValue:   portfolio.AssetComboValue(val),
			ExactMatch: exactMatch,
		}
	}

	b, err := json.Marshal(resData)
	if err != nil {
		errRes := generateErrorResponse(howler.CantWriteToJSON)
		w.WriteHeader(http.StatusInternalServerError)
		b, _ := json.Marshal(errRes)
		w.Write(b)
		return
	}
	if exactMatch {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusPartialContent)
	}
	w.Write(b)
	return

}

func generateErrorResponse(ec ...howler.Code) map[string][]howler.Code {
	return map[string][]howler.Code{
		"errors": ec,
	}
}
