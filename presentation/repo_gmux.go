package presentation

import (
	"adalpha-api/howler"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var baseRouter = mux.NewRouter()

func InitialiseRouter() {
	baseRouter.
		Methods("GET").
		Path("/portfolio/{ID}").
		HandlerFunc(GetPortfolio)

	baseRouter.
		Methods("GET").
		Path("/portfolio/dry/{funds}").
		HandlerFunc(GetWithdrawalOptions)

	baseRouter.Use(
		CORSMiddleware,
		JSONMiddleware,
	)

	log.Fatal(http.ListenAndServe(":8000", baseRouter))
}

func GetPortfolio(w http.ResponseWriter, r *http.Request) {
	if ID, ok := mux.Vars(r)["ID"]; ok {
		portfolioByID(ID, w)
		return
	}

	errRes := generateErrorResponse(howler.NoIDProvided)
	b, _ := json.Marshal(errRes)

	w.WriteHeader(http.StatusUnprocessableEntity)
	w.Write(b)
}

func GetWithdrawalOptions(w http.ResponseWriter, r *http.Request) {
	if fundsNeeded, ok := mux.Vars(r)["funds"]; ok {
		flFunds, err := strconv.ParseFloat(fundsNeeded, 64)
		if err != nil {
			errRes := generateErrorResponse(howler.WithdrawalAmountInvalid)
			b, _ := json.Marshal(errRes)
			w.WriteHeader(http.StatusUnprocessableEntity)
			w.Write(b)
		}
		withdrawalOptions(flFunds, w)
		return
	}
	errRes := generateErrorResponse(howler.NoIDProvided)
	b, _ := json.Marshal(errRes)

	w.WriteHeader(http.StatusUnprocessableEntity)
	w.Write(b)
}
